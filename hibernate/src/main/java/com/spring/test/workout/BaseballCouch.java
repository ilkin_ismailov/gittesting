package com.spring.test.workout;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;

public class BaseballCouch implements Couch {

    @Override
    public String dailyWorkout() {
        return "Baseball couch";
    }

    @Override
    public String getDailyFortune() {
        return "Get your daily baseball fortune.";
    }
}
