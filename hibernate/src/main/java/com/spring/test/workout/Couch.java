package com.spring.test.workout;

public interface Couch {

    String dailyWorkout();

    String getDailyFortune();
}
