package com.spring.test.workout;

public class TrackCoach implements Couch {

    @Override
    public String getDailyFortune() {
        return "Get your daily fortune!";
    }

    @Override
    public String dailyWorkout() {
        return "Track couch workout";
    }
}
