package com.spring.test.workout;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrackCoachTest {

    @Mock
    private TrackCoach trackCoach;

    @InjectMocks
    BaseballCouch baseballCouch;

    @Before
    public void setUp(){
    trackCoach = new TrackCoach();
    baseballCouch = new BaseballCouch();
    }

    @Test
    public void testingDailyWorkoutInTrackCouch(){
        assertThat(trackCoach.dailyWorkout()).isEqualTo("Track couch workout");
    }

    @Test
    public void tastingDailyWorkoutInBaseballCouch(){
        assertThat(baseballCouch.dailyWorkout()).isEqualTo("Baseball couch");
    }


}